=====================================================
==================Welcome to Read me==================
***********************************************
CouchDB  Username and Password
	Visit here: http://35.184.96.31:5984/_utils/#login
	Username: admin
	Password: root
***********************************************



Question: Why do we need to have a REST API for an existing API of CouchDB ?
ANSWER: បានជាយើងចំាំបាច់ត្រូវការ Rest Api ជំនួស API of CouchDB ដែលមានស្រាប់ព្រោះ
	- យើងអាចធ្វើការ Custom response និង  request ទៅតាមតម្រូវការ និងការងារជាប់ស្តែងទៅតាមតម្រូវការ
	-  Flexible ក្នុងការប្រើប្រាស់លើគ្រប់ palteform ដូចជា moblie app ,web ជាដើម
	- ងាយស្រួលក្នុង ការ update ដោយងាយក្នុងការ កែប្រែកូដ
	- ប្រើប្រៀបធៀប CocuhDB api ដំណើរការយឺតជាង api ធម្មតា ដោយ view មានលក្ខណៈយឺត

1- localhost:8080/api/v1/student/insert ​ (This end-point will auto insert student data to database by fetching student data from api link)
	*response 
	{
 		   "Message": "successfully insert student to database",
  		  "Request Time": "09-08-2022",
  		  "code": "200"
	}

2- localhost:8080/api/v1/student/delete-doc/1ff56c20c091d51e371ddfe6c50cfec2/1-cb6da1c7a4d15acd1738fee6aeefeb65
	(This end-point we required 2 param which is document Id and revision id by the use must provice docuId and revision id while request to  delete data *first param is document Id and last param is revison id as the link above )
	*response 
	{
    		"Message": "successfully delete student",
    		"Request Time": "09-08-2022",
    		"code": "200",
    		"deleted student": "2-7316265c4c53816888de22ce1ae88c2e"
	}


3- localhost:8080/api/v1/student/findById/1ff56c20c091d51e371ddfe6c50d2b2a
	(This end-point will the student information when the document id is match and it required only one param is document id)
*response 
	{
    		"_rev": "1-05d6aa600dc8a89a7bc5e4214885bbc6",
    		"studentid": 9,
    		"studentname": "SREY SITHARO",
    		"gender": "M",
    		"info": null,
    		"is_class_monitor": false,
    		"classname": "SR",
   		 "teamname": "DataAnalytics"
	}

4- localhost:8080/api/v1/student/update-doc/1ff56c20c091d51e371ddfe6c50d4626
	(This end-point is use for update student infomation and it is really simple user just give id and body(new student info in json form exampel:      {"name":"name update"} in this end-point if user update only name like example document will update only name and other data will save as normal)

*response 		
	{
    		"Message": "successfully Update student info",
    		"Request Time": "09-08-2022",
   		 "code": "200",
  		  "newRevision": "2-443753ef7e6833b2b07cc0598aaab214"
	}



5-localhost:8080/api/v1/student/findByRevID/2-443753ef7e6833b2b07cc0598aaab214
	(This end-point is use will be return student info by revision id )
{
    "Message": "successfully fetch data",
    "Request Time": "09-08-2022",
    "code": "200",
    "data": {
        "_rev": "2-443753ef7e6833b2b07cc0598aaab214",
        "studentid": 13,
        "studentname": "New update student",
        "gender": "",
        "info": null,
        "is_class_monitor": false,
        "classname": "",
        "teamname": ""
    }
}


6- localhost:8080/api/v1/student/filter-by-class/SR
	(This end-point is use will be return list of stdent by classname and it is requried only one param is classname)


{
    "Message": "successfully fetch data",
    "Request Time": "09-08-2022",
    "code": "200",
    "data": [
        {
            "_rev": "1-81ded50ed6e7cdb44cc9da487d9d0133",
            "studentid": 6,
            "studentname": "SANG SOKEA",
            "gender": "M",
            "info": null,
            "is_class_monitor": false,
            "classname": "SR",
            "teamname": "Blockchain"
        },
        {
            "_rev": "1-eaada0f090e81403edad74037afca4f2",
            "studentid": 1,
            "studentname": "CHAN SOK HEANG",
            "gender": "M",
            "info": null,
            "is_class_monitor": false,
            "classname": "SR",
            "teamname": "DataAnalytics"
        },
        {
            "_rev": "1-401facb95461419bfb05aecb9012f653",
            "studentid": 4,
            "studentname": "PHON SOPHEAP",
            "gender": "M",
            "info": null,
            "is_class_monitor": false,
            "classname": "SR",
            "teamname": "Devops"
        },
        {
            "_rev": "1-73e73d936c93cebd9e42fe4b1f92576e",
            "studentid": 5,
            "studentname": "PORET RITHYNEA",
            "gender": "M",
            "info": null,
            "is_class_monitor": false,
            "classname": "SR",
            "teamname": "Devops"
        },
        {
            "_rev": "1-31e7fcf395c955790d0e623d2ba145f8",
            "studentid": 7,
            "studentname": "SANG SOLIMA",
            "gender": "F",
            "info": null,
            "is_class_monitor": false,
            "classname": "SR",
            "teamname": "DataAnalytics"
        },
        {
            "_rev": "1-83223598a9584dfee28b0d93a42ea79a",
            "studentid": 8,
            "studentname": "SARY MANIDA",
            "gender": "F",
            "info": null,
            "is_class_monitor": false,
            "classname": "SR",
            "teamname": "Blockchain"
        },
        {
            "_rev": "1-05d6aa600dc8a89a7bc5e4214885bbc6",
            "studentid": 9,
            "studentname": "SREY SITHARO",
            "gender": "M",
            "info": null,
            "is_class_monitor": false,
            "classname": "SR",
            "teamname": "DataAnalytics"
        },
        {
            "_rev": "1-57b7d7ca24333eb8b432da001ead8a45",
            "studentid": 10,
            "studentname": "TRY KRISNA",
            "gender": "M",
            "info": null,
            "is_class_monitor": false,
            "classname": "SR",
            "teamname": "Blockchain"
        },
        {
            "_rev": "1-554d0f092c402986fed3c8c26b700232",
            "studentid": 11,
            "studentname": "TRY POUYY",
            "gender": "M",
            "info": null,
            "is_class_monitor": false,
            "classname": "SR",
            "teamname": "Devops"
        },
        {
            "_rev": "1-142a4569b7be17f27adec6df5a364742",
            "studentid": 12,
            "studentname": "VONG YUOYI",
            "gender": "F",
            "info": null,
            "is_class_monitor": false,
            "classname": "SR",
            "teamname": "DataAnalytics"
        }
    ]
}




















# Delete end-point
http://35.184.96.31:5984/db_student/_design/findByRev/_view/get-by-rev/?rev=2-6336ae0faf8f3a675d83841cb43f80a5

localhost:8080/api/v1/student/delete-doc/1ff56c20c091d51e371ddfe6c505189d/1-eaada0f090e81403edad74037afca4f2


{
  "_id": "_design/getdoc",
  "_rev": "1-675ce78900e3c87e98e3ee3bd574f5dd",
  "views": {
    "get-by-rev": {
      "map": "function (doc) {\n  emit(doc._rev, doc);\n}"
    }
  },
  "language": "javascript"
}